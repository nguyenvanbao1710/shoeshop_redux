const stateGioHang = {
  gioHang: [],
};

export const GioHangReducer = (state = stateGioHang, action) => {
  switch (action.type) {
    case "THEM_GIO_HANG": {
      //xu ly logic them gio hang
      let gioHangCapNhat = [...state.gioHang];
      let index = gioHangCapNhat.findIndex(
        (spGH) => spGH.maSP == action.spGioHang.maSP
      );
      if (index !== -1) {
        gioHangCapNhat[index].soLuong += 1;
      } else {
        gioHangCapNhat.push(action.spGioHang);
      }
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "XOA_GIO_HANG": {
      let gioHangCapNhat = [...state.gioHang];
      //xoa gio hang dua vao index
      gioHangCapNhat.splice(action.index, 1);
      //gan gio hang moi cho state.gioHang => render lai giao dien
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "TANG_GIAM_SL": {
      const { index, tangGiam } = action;
      let gioHangCapNhat = [...state.gioHang];
      if (tangGiam) {
        gioHangCapNhat[index].soLuong += 1;
      } else {
        if (gioHangCapNhat[index].soLuong > 1) {
          gioHangCapNhat[index].soLuong -= 1;
        } else {
          gioHangCapNhat.splice(index, 1);
        }
      }
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    default:
  }
  return { ...state };
};
