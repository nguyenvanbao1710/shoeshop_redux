import { combineReducers } from "redux";
import { GioHangReducer } from "./GioHangReducer";

//store tong ung dung
export const rootReducer = combineReducers({
  //noi se chua cac reducer cho nghiep vu
  GioHangReducer: GioHangReducer,
});
