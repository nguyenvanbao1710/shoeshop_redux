import React, { Component } from "react";

//ket noi redux (connect ham ket noi reactComponent - reduxStore)
import { connect } from "react-redux";

class ModalGioHang extends Component {
  renderGioHang = () => {
    return this.props.gioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.maSP}</td>
          <td>{item.tenSP}</td>
          <td>
            <img src={item.hinhAnh} width={75} height={70} />
          </td>
          <td>{item.giaBan}</td>
          <td>
            <button
              className="btn btn-light"
              onClick={() => {
                this.props.tangGiamSoLuong(index, true);
              }}
            >
              +
            </button>
            {item.soLuong}
            <button
              className="btn btn-light"
              onClick={() => {
                this.props.tangGiamSoLuong(index, false);
              }}
            >
              -
            </button>
          </td>
          <td>{item.soLuong * item.giaBan}</td>
          <td>
            <button
              onClick={() => {
                this.props.xoaGioHang(index);
              }}
              className="btn btn-danger"
            >
              delete
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Ma sp</th>
              <th>Ten sp</th>
              <th>Hinh anh</th>
              <th>Gia ban</th>
              <th>So luong</th>
              <th>Thanh tien</th>
            </tr>
          </thead>
          <tbody>{this.renderGioHang()}</tbody>
          <tfoot>
            <td colSpan={5}></td>
            <td>Total</td>
            <td>
              {this.props.gioHang.reduce((tongTien, spGioHang, index) => {
                return (tongTien += spGioHang.soLuong * spGioHang.giaBan);
              }, 0)}
            </td>
          </tfoot>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gioHang: state.GioHangReducer.gioHang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHang: (index) => {
      const action = {
        type: "XOA_GIO_HANG",
        index,
      };
      //dua action le reducer
      dispatch(action);
    },
    tangGiamSoLuong: (index, tangGiam) => {
      const action = {
        type: "TANG_GIAM_SL",
        index,
        tangGiam,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalGioHang);
