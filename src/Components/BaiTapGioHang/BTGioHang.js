import React, { Component } from "react";
import DanhSachSanPham from "./DanhSachSanPham";
import ModalGioHang from "./ModalGioHang";

class BTGioHang extends Component {
  render() {
    return (
      <div className="container">
        <h3 className="text-center text-danger mt-2">Bài tập giỏ hàng redux</h3>
        <ModalGioHang />
        <DanhSachSanPham />
      </div>
    );
  }
}

export default BTGioHang;
