import React, { Component } from "react";
import { connect } from "react-redux";

class SanPham extends Component {
  render() {
    const { sanPham } = this.props;

    return (
      <div>
        <div className="card">
          <img
            className="card-img-top"
            src={sanPham.hinhAnh}
            width={100}
            height={250}
          />
          <div className="card-body">
            <h4 className="card-title">{sanPham.tenSP}</h4>
            <p className="card-text">{sanPham.giaBan}</p>
            <button
              onClick={() => {
                this.props.themGioHang(sanPham);
              }}
              className="btn btn-danger"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    //tao ra props component laf function => dua du lieu len store
    themGioHang: (sanPham) => {
      const spGioHang = {
        maSP: sanPham.maSP,
        tenSP: sanPham.tenSP,
        giaBan: sanPham.giaBan,
        soLuong: 1,
        hinhAnh: sanPham.hinhAnh,
      };
      //tao action dua du lieu len reducer
      const action = {
        type: "THEM_GIO_HANG", //bat buoc dat type
        spGioHang: spGioHang, //noi dung gui len reducer
      };
      //dung ham dispatch dua lieu action len reducer
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(SanPham);
